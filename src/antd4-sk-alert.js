
import { AntdSkAlert }  from '../../sk-alert-antd/src/antd-sk-alert.js';

export class Antd4SkAlert extends AntdSkAlert {

    get prefix() {
        return 'antd4';
    }

}
